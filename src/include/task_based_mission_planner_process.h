/*!*****************************************************************************
 *  \file    task_based_mission_planner_process.h
 *  \brief   Definition of all the classes used in the file
 *           task_based_mission_planner_process.cpp .
 *   
 *  \author  Adrian Diaz
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *  
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *  
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *  
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/
#ifndef TASK_BASED_MISSION_PLANNER
#define TASK_BASED_MISSION_PLANNER

#include <stdio.h>
#include "Mission.h"
#include "EventHandler.h"
#include "Parameter_Handler.h"

 /// ROS ///
#include "ros/ros.h"

/// Messages ///
#include "std_msgs/Bool.h"
#include "droneMsgsROS/battery.h"
#include "droneMsgsROS/obsVector.h"
#include "droneMsgsROS/distancesToObstacles.h"
#include "droneMsgsROS/actionData.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/CompletedAction.h"

/// Services ///
#include "std_srvs/Empty.h"
#include "droneMsgsROS/skillRequest.h"
#include "droneMsgsROS/setInitDroneYaw_srv_type.h"
#include "droneMsgsROS/openMissionFile.h"
#include "droneMsgsROS/missionName.h"

///Drone process///
#include "drone_process.h"

/*!***************************************************************************
 *  \class TaskBasedMissionPlanner
 *  \brief This class represents an abstraction of a mission schedule.
 *****************************************************************************/
class TaskBasedMissionPlanner: public DroneProcess
{ 

private:
  Mission      * current_mission;
  EventHandler * event_handler;
  //To save the current task is very important.
  Task         * current_task;
  // Parameter Handler
  Parameter_Handler parameter_handler;
  // Since the tree of the current mission is modified dynamically, it is 
  // necessary to store the original mission and event handler. 
  TreeNode<Task>     original_mission_task_tree;
  std::vector<Event> original_event_vector;
  //It is necessary to remember what task was beeing executed before the 
  //activation of an event
  Task  task_before_event;
  //The same with the skills.
  std::vector<SkillType> skills_activated_before_event;
  //Flags, very important to control the correct execution of the program.
  bool file_loaded;
  bool start_mission_flag;
  bool action_completed;
  bool action_completed_sucessfully;
  bool mission_ended_by_user;
  //Remembered points
  std::vector<argument<std::string, std::vector<double>>> remembered_points;
  //Current pose
  point current_pose;//<-- This should be included in the table of parameters
  //Table of parameters.
  std::vector<std::pair<std::string,std::string>> table_of_parameters;
  //Mission's xml file path
  std::string mission_path;
  //Topic Names
  std::string droneInitiatedActionTopicName;
  std::string droneCompletedActionTopicName;
  std::string droneRequestedActionTopicName;
  std::string droneArucoObservationTopicName;
  std::string droneBatteryTopicName;
  std::string droneCurrentPoseTopicName;
  std::string droneCurrentTaskTopicName;
  std::string droneCompletedMissionTopicName;
  //ROS Node Handler
  ros::NodeHandle n;
  //Subscriber variables
  ros::Subscriber droneInitiatedActionSub;
  ros::Subscriber droneCompletedActionSub;
  ros::Subscriber droneArucoObservationSub;
  ros::Subscriber droneBatterySub;
  ros::Subscriber droneCurrentPoseSub;
  //Publisher variables
  ros::Publisher droneRequestedActionPub;
  ros::Publisher droneCurrentTaskPub;
  ros::Publisher droneCompletedMissionPub;
  //Service client variables
  ros::ServiceClient skillRequestClient;
  //Service server variables
  ros::ServiceServer openMissionFileServer;
  ros::ServiceServer missionNameServer;
   
public:
  //Constructor & Destructor.
  TaskBasedMissionPlanner();
  ~TaskBasedMissionPlanner();
   
  //DroneProcess functions.
  void ownSetUp();
  void ownStart();
  void ownStop();
  void ownRun();

//Auxiliar methods
private:
  /*!************************************************************************
   *  \brief  This method sends a new task through the topics this module
   *          is connected.
   *************************************************************************/
  void sendNewTask();
  /*!************************************************************************
   *  \brief  This method reads the parameters needed from the .launch file.
   *************************************************************************/
  void getParametersFromFile();
  /*!************************************************************************
   *  \brief  When an error occurs, this method is called to abort 
   *          the main mission.
   *************************************************************************/
  void abortMission();
  /*!************************************************************************
   *  \brief  This method finishes the main mission. It is only executed when 
   *          the user specifies that the mission must end.
   *************************************************************************/
  void endMission();
  
  //Topic Callbacks.
  void droneApprovedActionCallback(const droneMsgsROS::actionData &msg);
  void droneCompletedActionCallback(const droneMsgsROS::CompletedAction &msg);
  void droneArucoObservationCallback(const droneMsgsROS::obsVector &msg);
  void droneBatteryCallback(const droneMsgsROS::battery &msg);
  void droneCurrentPoseCallback(const droneMsgsROS::dronePose &msg);

  //Service functions 
  bool openMissionFile(droneMsgsROS::openMissionFile::Request &req, droneMsgsROS::openMissionFile::Response &res);
  bool missionName(droneMsgsROS::missionName::Request &req, droneMsgsROS::missionName::Response &res);
};
#endif
