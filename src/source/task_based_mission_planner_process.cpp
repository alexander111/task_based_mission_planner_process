#include "task_based_mission_planner_process.h"
using namespace std;

TaskBasedMissionPlanner::TaskBasedMissionPlanner():DroneProcess()
{  
  //Initialize flags
  file_loaded = false;
  start_mission_flag = false;
  mission_ended_by_user = false;
}

TaskBasedMissionPlanner::~TaskBasedMissionPlanner()
{

}

void TaskBasedMissionPlanner::abortMission()
{
  std_msgs::Bool completed_mission_msg;
  completed_mission_msg.data = false;
  droneCompletedMissionPub.publish(completed_mission_msg);
  cout << "\033[1;31mMission aborted.\n\033[0m"<<endl;
  stop();
}

void TaskBasedMissionPlanner::endMission()
{
  std_msgs::Bool completed_mission_msg;
  completed_mission_msg.data = true;
  mission_ended_by_user = true;
  droneCompletedMissionPub.publish(completed_mission_msg);
  cout << "\033[1;34m--------------- MISSION ENDED ---------------\033[0m\n" << endl;
  stop();
}

void TaskBasedMissionPlanner::sendNewTask()
{
  int mpAction_to_send;
  EndingStepType event_ending_step;
  std::vector<SkillType> active_skills;
  std::vector<argument<std::string,std::vector<double>>>  arguments_to_send;
  std::string skill_name_to_send;
  std::string aux_message = "";
  //Action duration
  double action_duration = 0;
  //Message to fill with information of the current action.
  droneMsgsROS::actionData msg;
  //Message where the name of the current task is going to be stored.
  std_msgs::String current_task_msg;
  //Message where the arguments of the action are going to be stored.
  droneMsgsROS::actionArguments arg_msg;
  //Service request.
  droneMsgsROS::skillRequest srv;
  
  //First, it is mandatory to check if an event has been activated.
  if(!event_handler->activeEvent())
  {
    //Body for "regular" tasks.
    //Skills needed to be activated.
    skills_activated_before_event = current_mission->getActiveSkills();
    active_skills= skills_activated_before_event;
    //Save current task 
    task_before_event = current_mission->nextTask();
    //Current task to perform
    *current_task = task_before_event;
    //The mission may have been completed during the execution of function nextTask due to 
    //the evaluation of the conditions declared within the tasks.
    if(current_task->getAction()->isEmpty()) return;
    parameter_handler.updateTableOfParameters(std::make_pair(PAR_CURRENT_TASK, current_task->getAbsoluteName()));
    //Action's arguments.
    arguments_to_send = current_task->getAction()->getArguments();
  }
  //Checking if the current task raises an event
  event_handler->checkEvents(parameter_handler);
  if(event_handler->activeEvent())
  {
    // Body for events
    // If there is not a next action to perform, then the termination mode should 
    // be executed. current_task is initialized by the function nextTask.

    if(!event_handler->nextTask(current_task))
    {   
      event_ending_step = event_handler->getCurrentEventEndingStep();
      //Executing ending step...
      cout << "Executing ending step..." << endl;
      switch(event_ending_step)
      {
        case EndingStepType::repeatTask:
          //Reset event handler and repeat the task that
          //the mission planner was executing before the event activation.
          event_handler->resetEventHandler();
          *current_task = task_before_event;
          active_skills = skills_activated_before_event;
          arguments_to_send = current_task->getAction()->getArguments();
          break;
        case EndingStepType::nextTask:
          //Reset event handler and go back to the main running loop.
          event_handler->resetEventHandler();
          //Event finished.
          return;
        case EndingStepType::abortMission:
          event_handler->resetEventHandler();
          abortMission();
          //Event finished.
          return;      
        case EndingStepType::endMission:
          event_handler->resetEventHandler();
          endMission();
          return;
        default:
          event_handler->resetEventHandler();
          //Event finished.
          return;
      }
    }
    else
    {
      //Skills needed to be activated.
      active_skills = *current_task->getSkills();
      //Action's arguments.   
      arguments_to_send = current_task->getAction()->getArguments();
    } 
  }

  cout << "Trying to perform "<< current_task->getAction()->getReadableName() << "..." << endl;
  //It is important to send first the active skills one by one.
  //That is the way the skill manager expects to receive the needed skills.

  for(uint i = 0; i<active_skills.size(); i++)
  {
    switch(active_skills.at(i))
    {    
      case SkillType::avoidingObstacles:
        skill_name_to_send = BHV_AVOIDING_OBSTACLES;
        break; 
      case SkillType::recognizingArucoMarkers:
        skill_name_to_send = BHV_RECOGNIZING_ARUCO_MARKERS;
        break; 
      case SkillType::recognizingVisualMarkers:
        skill_name_to_send = BHV_RECOGNIZING_VISUAL_MARKERS;
        break; 
      case SkillType::sayingOutLoudCurrentTask:
        skill_name_to_send = BHV_SAYING_OUT_LOUD_CURRENT_TASK;
        break;         
    }
    //Setting service values.
    srv.request.skill_name = skill_name_to_send;
    //By convention, the skill not specified in the service is understood to be deactivated.
    srv.request.activate = true;
    //Call and wait for response
    if (!skillRequestClient.call(srv) || !srv.response.ack){
      cout << "\033[0;31m[ERROR] Skill " << skill_name_to_send << " could not be sent. "<< srv.response.why <<"\nAborting mission...\033[0m"<<endl;
      abortMission();
      return;
    }
  }
  //Getting action from task.
  switch(current_task->getAction()->getName())
  {
    case ActionType::takeOff:
      mpAction_to_send = droneMsgsROS::actionData::TAKE_OFF;
      break;
    case ActionType::hover:
      mpAction_to_send = droneMsgsROS::actionData::HOVER;
      break;
    case ActionType::land:
      mpAction_to_send = droneMsgsROS::actionData::LAND;
      break;
    case ActionType::stabilize:
      mpAction_to_send = droneMsgsROS::actionData::STABILIZE;
      break;
    case ActionType::positionMovement:
      mpAction_to_send = droneMsgsROS::actionData::MOVE;
      break;
    case ActionType::goToPoint:
      mpAction_to_send = droneMsgsROS::actionData::GO_TO_POINT;
      break;
    case ActionType::rotateYaw:
      mpAction_to_send = droneMsgsROS::actionData::ROTATE_YAW;
      break;
    case ActionType::flipMovementRight:
      mpAction_to_send = droneMsgsROS::actionData::FLIP_RIGHT;
      break;
    case ActionType::flipMovementLeft:
      mpAction_to_send = droneMsgsROS::actionData::FLIP_LEFT;
      break;
    case ActionType::flipMovementFront:
      mpAction_to_send = droneMsgsROS::actionData::FLIP_FRONT;
      break;
    case ActionType::flipMovementBack:
      mpAction_to_send = droneMsgsROS::actionData::FLIP_BACK;
      break;
    case ActionType::rememberPoint:
      mpAction_to_send = -1; //Actions signed with -1 are considered inner actions that does not need to be sent to any other module.
      argument<std::string, std::vector<double>> point_to_remember;
      point_to_remember.name   = arguments_to_send.at(0).name;
      point_to_remember.value = {current_pose.x, current_pose.y, current_pose.z};
      //If the point has been rememebered before, then its value is overwriten.
      if(remembered_points.size() > arguments_to_send.at(0).value.at(0))  
        remembered_points.at(arguments_to_send.at(0).value.at(0)).value = point_to_remember.value;
      else 
        remembered_points.push_back(point_to_remember);
      //Time to inform the user that the current point has been memorized.
      cout << "Memorizing point (" << point_to_remember.value.at(0) << ", " << point_to_remember.value.at(1)<< ", " << point_to_remember.value.at(2) <<")." << endl;
      break;       
  }
  // There are "special" actions that are not necessary to be sent.
  if(mpAction_to_send >= 0)
  {
    //Setting up the first parameter in the message.
    msg.mpAction = mpAction_to_send;
    //Setting up arguments
    for(uint i = 0; i < arguments_to_send.size() ; i++)
    { 
      if(arguments_to_send.at(i).name == NAME_DURATION)
      {
        arg_msg.argumentName = droneMsgsROS::actionArguments::DURATION;
        for(uint j = 0; j < arguments_to_send.at(i).value.size(); j++)
          arg_msg.value.push_back(arguments_to_send.at(i).value.at(j));
      }
      else if(arguments_to_send.at(i).name == NAME_DESTINATION && arguments_to_send.at(i).value_string == ATTR_VALUE)
      {
        arg_msg.argumentName = droneMsgsROS::actionArguments::DESTINATION;
        for(uint j = 0; j < arguments_to_send.at(i).value.size(); j++)
          arg_msg.value.push_back(arguments_to_send.at(i).value.at(j));
      }
      else if(arguments_to_send.at(i).name == NAME_ROTATION)
      { 
        arg_msg.argumentName = droneMsgsROS::actionArguments::ROTATION;
        for(uint j = 0; j < arguments_to_send.at(i).value.size(); j++)
          arg_msg.value.push_back(arguments_to_send.at(i).value.at(j));
      }
      else if(arguments_to_send.at(i).name == NAME_DESTINATION && arguments_to_send.at(i).value_string ==ATTR_LABEL)
      {
        arg_msg.value = remembered_points.at(arguments_to_send.at(i).value.at(0)).value;
        arg_msg.argumentName = droneMsgsROS::actionArguments::DESTINATION;
      }
      msg.arguments.push_back(arg_msg);
      arg_msg.value.clear();
    }
    //Setting up time
    msg.time = ros::Time::now();
    //Send task action and its arguments.
    droneRequestedActionPub.publish(msg);
    //Publish current task.
    current_task_msg.data = current_task->getAbsoluteName();
    droneCurrentTaskPub.publish(current_task_msg);    
  }
  //The task has been completed, so a new task has to be sent. (This kind of actions are quite special since it is not necessary to publish any message)
  else
  {
    action_completed = true;
    action_completed_sucessfully = true;
    cout << "\033[1;34m--------------- ACTION COMPLETED! ---------------\033[0m" << endl;
    sendNewTask();
  }
}

void TaskBasedMissionPlanner::droneApprovedActionCallback(const droneMsgsROS::actionData &msg)
{
  if(!msg.ack){
    action_completed = false;
    action_completed_sucessfully=false;
    cout << "\033[0;31m[ERROR] The action has not been approved.\n Aborting mission...\033[0m"<<endl;
  }
} 

void TaskBasedMissionPlanner::droneCompletedActionCallback(const droneMsgsROS::CompletedAction &msg)
{
  action_completed = true;
  if(msg.final_state ==  droneMsgsROS::CompletedAction::SUCCESSFUL)
  {
    cout << "\033[1;34m--------------- ACTION COMPLETED! ---------------\033[0m" << endl;
    action_completed_sucessfully=true;  
  }
  else if(msg.final_state ==  droneMsgsROS::CompletedAction::TIMEOUT_ACTIVATED)
  {
    cout << "\033[1;31m---------- TIME FOR ACTION HAS RUN OUT! ---------\033[0m" << endl;
    action_completed_sucessfully=false;
  }
  else if(msg.final_state ==  droneMsgsROS::CompletedAction::INTERRUPTED)
  {
    cout << "\033[1;31m-------------- ACTION INTERRUPTED! --------------\033[0m" << endl;
    action_completed_sucessfully=false;
  }
  else
  {
    cout << "\033[1;31m----------------- ACTION FAILED! ----------------\033[0m" << endl;
    action_completed_sucessfully=false;
  }
}

void TaskBasedMissionPlanner::droneArucoObservationCallback(const droneMsgsROS::obsVector &msg)
{
  //Checking if an event has to be activated.
  std::vector<droneMsgsROS::Observation3D> aux_vec = msg.obs;
  std:vector<int> id_vector;

  for(uint i=0; i< aux_vec.size(); i++)
    id_vector.push_back(aux_vec.at(i).id);
 
  parameter_handler.updateTableOfParameters(std::make_pair(PAR_ARUCO_VISUAL_MARKER, parameter_handler.fromIntVectorToString(id_vector)));
  if(file_loaded && !aux_vec.empty() && event_handler->checkEvents(parameter_handler))
    sendNewTask();
}
void TaskBasedMissionPlanner::droneBatteryCallback(const droneMsgsROS::battery &msg)
{ 
  //Checking if an event has to be activated.
  parameter_handler.updateTableOfParameters(std::make_pair(PAR_BATTERY_CHARGE_PERCENTAGE, parameter_handler.fromNumberToString(msg.batteryPercent)));
  if(file_loaded && event_handler->checkEvents(parameter_handler))
    sendNewTask();
}
void TaskBasedMissionPlanner::droneCurrentPoseCallback(const droneMsgsROS::dronePose &msg)
{
  if(file_loaded)
  {
  //Saving current position
  current_pose.x = msg.x;
  current_pose.y = msg.y;
  current_pose.z = msg.z;
  }
}

bool TaskBasedMissionPlanner::missionName(droneMsgsROS::missionName::Request &req, droneMsgsROS::missionName::Response &res)
{
  if(file_loaded)
  {
    res.mission_name = current_mission->getName();
    return true;
  }
  return false;
}

bool TaskBasedMissionPlanner::openMissionFile(droneMsgsROS::openMissionFile::Request &req, droneMsgsROS::openMissionFile::Response &res)
{
  mission_path = req.mission_file_path;
  //Initialize mission attributes.
  Parser parser(mission_path);
  TreeNode<Task> generatedTaskTree = parser.generateTaskTree();
  std::vector<Event> generatedEventVector = parser.generateEventVector();
  file_loaded = parser.getErrors().empty();
  if(file_loaded)
  {
    current_mission  = new Mission(generatedTaskTree, parameter_handler);
    event_handler    = new EventHandler(generatedEventVector);
    res.ack          = true;
    res.mission_name = current_mission->getName();
  }
  else
  {
    res.ack            = false;
    res.error_messages = parser.getErrors();
    parser.displayFoundErrors();
  }
  return true; 
}

void TaskBasedMissionPlanner::ownSetUp()
{
  //Mission's file name.
  std::string aux;
  ros::param::get("~mission_config_file", mission_path);

  ros::param::get("~droneId", aux);
  mission_path = aux+"/"+mission_path;

  ros::param::get("~stackPath", aux);
  mission_path = aux+"/configs/drone"+mission_path;

  /// SUBSCRIBERS ///
  ros::param::get("~drone_Initiated_Action_Topic_Name", droneInitiatedActionTopicName);
  if ( droneInitiatedActionTopicName.length() == 0)
  {
    droneInitiatedActionTopicName = "initiated_action";
  }
  ros::param::get("~drone_Completed_Action_Topic_Name", droneCompletedActionTopicName);
  if ( droneCompletedActionTopicName.length() == 0)
  {
    droneCompletedActionTopicName = "completed_action";
  }
  ros::param::get("~drone_Aruco_Observation_Topic_Name", droneArucoObservationTopicName);
  if ( droneArucoObservationTopicName.length() == 0)
  {
    droneArucoObservationTopicName = "arucoObservation";
  }
  ros::param::get("~drone_Battery_Topic_Name", droneBatteryTopicName);
  if ( droneBatteryTopicName.length() == 0)
  {
    droneBatteryTopicName = "battery";
  }
  ros::param::get("~drone_Current_Pose_Topic_Name", droneCurrentPoseTopicName);
  if ( droneCurrentPoseTopicName.length() == 0)
  {
    droneCurrentPoseTopicName = "EstimatedPose_droneGMR_wrt_GFF";
  }
  /// PUBLISHERS ///
  ros::param::get("~drone_Requested_Action_Topic_Name", droneRequestedActionTopicName);
  if ( droneRequestedActionTopicName.length() == 0)
  {
    droneRequestedActionTopicName="requested_action";
  }
  ros::param::get("~drone_Current_Task_Topic_Name", droneCurrentTaskTopicName);
  if ( droneCurrentTaskTopicName.length() == 0)
  {
    droneCurrentTaskTopicName="current_task";
  }
  ros::param::get("~drone_Completed_Mission_Topic_Name", droneCompletedMissionTopicName);
  if ( droneCompletedMissionTopicName.length() == 0)
  {
    droneCompletedMissionTopicName="completed_mission";
  }
  //Initialize mission attribute.
  Parser parser(mission_path);
  TreeNode<Task> generatedTaskTree = parser.generateTaskTree();
  std::vector<Event> generatedEventVector = parser.generateEventVector();
  //If errors have occur, the mission cannot continue.
  if(!parser.getErrors().empty())
  {
    parser.displayFoundErrors();
    exit(1);
  }
  //File loaded without errors
  file_loaded = true; 
  original_mission_task_tree = generatedTaskTree;
  original_event_vector      = generatedEventVector;
  current_mission = new Mission(generatedTaskTree, parameter_handler);
  event_handler   = new EventHandler(generatedEventVector);

  //This service must be operative always
  missionNameServer = n.advertiseService("mission_name" , &TaskBasedMissionPlanner::missionName, this);

  cout << "\033[1;32mFile loaded without errors! Waiting to start ...\033[0m" << endl;
}

void TaskBasedMissionPlanner::ownStart()
{
  //Subscribing to topics
  droneInitiatedActionSub  = n.subscribe(droneInitiatedActionTopicName, 1, &TaskBasedMissionPlanner::droneApprovedActionCallback, this);
  droneCompletedActionSub  = n.subscribe(droneCompletedActionTopicName, 1, &TaskBasedMissionPlanner::droneCompletedActionCallback, this);
  droneArucoObservationSub = n.subscribe(droneArucoObservationTopicName, 1, &TaskBasedMissionPlanner::droneArucoObservationCallback, this);
  droneBatterySub          = n.subscribe(droneBatteryTopicName, 1, &TaskBasedMissionPlanner::droneBatteryCallback, this);
  droneCurrentPoseSub      = n.subscribe(droneCurrentPoseTopicName, 1, &TaskBasedMissionPlanner::droneCurrentPoseCallback, this);
  //Publishing in topics
  droneRequestedActionPub  = n.advertise<droneMsgsROS::actionData>(droneRequestedActionTopicName, 1, true);
  droneCurrentTaskPub      = n.advertise<std_msgs::String>(droneCurrentTaskTopicName, 1, true);
  droneCompletedMissionPub = n.advertise<std_msgs::Bool>(droneCompletedMissionTopicName, 1, true);
  //Servicies
  skillRequestClient       = n.serviceClient<droneMsgsROS::skillRequest>("request_skill");
  openMissionFileServer    = n.advertiseService("open_mission_file" , &TaskBasedMissionPlanner::openMissionFile, this);

  //Just to start sending the first task.
  action_completed = true;
  action_completed_sucessfully=true;
  //Allocating memory for the new task.
  current_task      = new Task();
  current_mission   = new Mission(original_mission_task_tree, parameter_handler);
  event_handler     = new EventHandler(original_event_vector);
  file_loaded       = true;
  //Printing mission tree.
  current_mission -> printTree();
  event_handler   -> printEventVector(); 
  //Everything is ready to start the mission
}

void TaskBasedMissionPlanner::ownStop()
{
  droneInitiatedActionSub.shutdown();
  droneCompletedActionSub.shutdown();  
  droneArucoObservationSub.shutdown();
  droneBatterySub.shutdown();
  droneCurrentPoseSub.shutdown();

  droneRequestedActionPub.shutdown();
  droneCurrentTaskPub.shutdown();
  droneCompletedMissionPub.shutdown();

  skillRequestClient.shutdown();
  openMissionFileServer.shutdown();

  //Deallocate memory and reset values;
  start_mission_flag = false;
  action_completed = false;
  action_completed_sucessfully = false;
  //If the file has been loaded, it is necessary to deallocate the memory used.
  if(file_loaded)
  {
    delete current_task;
    delete current_mission;
    delete event_handler;
    file_loaded =false;
  }
}

void TaskBasedMissionPlanner::ownRun()
{
  if(!current_mission->missionCompleted() && !mission_ended_by_user)
  {
    start_mission_flag = true;
    if (!action_completed_sucessfully)
      abortMission();
    if(action_completed)
    {
      sendNewTask();
      action_completed = false;
    }
  }
  else
  {
    std_msgs::Bool completed_mission_msg;
    completed_mission_msg.data = true;
    droneCompletedMissionPub.publish(completed_mission_msg);
    start_mission_flag = false;
    cout << "\033[1;34m--------------- MISSION COMPLETED! ---------------\033[0m\n" << endl;
    stop();
  }
}
