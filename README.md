#Aerostack's Process: Task-based Mission Planner 

This package is an Aerostack's process for mission planning. 
The operator can specify a mission in TML language using a task-based approach and this process interprets such
a specification generating the appropriate orders to the rest of process in Aerostack to 
carry out the mission.

---
 
##ROS node: task_based_mission_planner_process

###1. Subscribed Topics

- *requested_action* (message droneMsgsROS/RequestedAction): Action that is currently being performed.

- *completed_action* (message droneMsgsROS/CompletedAction): Last action completed.

- *battery* (message droneMsgsROS/battery): State of the battery.

###2. Published Topics

- *current_task* (message droneMsgsROS/CurrentTask): Current task executed by the mission planner.

- *mission_state* (message droneMsgsROS/MissionState): Current state of the mission execution.

###3. Services Requested

- *request_skill*: Skills requested to be active.

###4. Parameters

None

###5. Configuration Files

- *mission_specification_file.xml*: The mission specification is written in a XML file, called mission_specification_file.xml, using the TML language. 

---

##Publication

Martin Molina, Adrian Diaz-Moreno, David Palacios, Ramon A. Suarez-Fernandez, Jose Luis Sanchez-Lopez,
Carlos Sampedro, Hriday Bavle and Pascual Campoy (2016): "Specifying Complex Missions for Aerial Robotics in
Dynamic Environments". International Micro Air Vehicle Conference and Competition, IMAV 2016. Beijing, China.

---
##Maintainer and Authors
  
**Maintainer**: Adrian Diaz <adrian.diazm@outlook.com>

**Authors**:

- Adrian Diaz (design, programming, testing)

- Martin Molina (specification, design)